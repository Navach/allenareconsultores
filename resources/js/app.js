require('./bootstrap');
window.Vue = require('vue');

// authenticacion
Vue.component('login', require('./components/Auth/login.vue').default);
Vue.component('social', require('./components/Auth/social.vue').default);
Vue.component('recuperar', require('./components/Auth/recuperar-contraseña').default);


// usuarios
Vue.component('form-usuarios', require('./components/Usuarios/form-usuarios.vue').default);
Vue.component('tabla-usuarios', require('./components/Usuarios/tabla_usuarios.vue').default);



//Linea de tiempo
Vue.component('TimeLine', require('./components/Time/TimeLine.vue').default);


// proyectos
Vue.component('form-proyectos', require('./components/Proyectos/form-proyectos.vue').default);


// Documentos
Vue.component('perfil', require('./components/Docs/perfil.vue').default);
Vue.component('manual', require('./components/Docs/manual.vue').default);


Vue.component('fileInput', require('./components/inputs/FileInput').default);

const app = new Vue({
    el: '#app'
});

