@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            Swal.fire(
            'Terminos y condiciónes',
            'Uso correcto de la aplicación web en base a la Ley Federal de Protección de Datos,
            la Ley Federal del Consumidor, la Ley Federal de Derechos de Autor y las políticas
            propias que regulan las actividades en Internet. Como a su vez bajo las normas y reglamentos de la empresa Allenare Consultores.',
            'question'
            )
        </div>
    </div>
</div>
@endsection
