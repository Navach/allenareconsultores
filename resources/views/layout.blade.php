<!DOCTYPE html>
<html lang="es">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Christian Navarro">
    <meta name="keyword" content="">
    <title>Allenare Consultores</title>

    <link href="{{ asset('admin/vendors/@coreui/icons/css/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/vendors/@fortawesome/fontawesome-free/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/application.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet">

    <script src="{{ asset('admin/vendors/jquery/dist/jquery.min.js') }}"></script>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

<!--Nav bar -->
@include('layouts/NavBar')

<div class="app-body">
    <!-- Menu izquierdo-->
    @include('layouts/Sidebar-Left')

    <main class="main">
        <!-- sub nav bar -->

        {{ \Breadcrumbs::render($page, isset($parameter) ? $parameter: null) }}


        <div class="container-fluid">
            <div id='app'>
                <div id="contenido">
                    <div class="animated fadeIn"></div>
                    @yield('contenido')
                </div>
            </div>
        </div>
    </main>

    <!-- Menu derecho -->
    @include('layouts/Sidebar-Right')

</div>
<!-- Pie de pagina -->
@include('layouts/Footer')


</body>


{{-- Si tiene esa ruta no pone esto --}}
@if (\  Route::current()->getName() != 'Calendario')
    <script src="{{ asset('js/app.js') }}"></script>
@endif


<!-- CoreUI and necessary plugins-->
<script src="{{ asset('admin/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/vendors/pace-progress/pace.min.js') }}"></script>
<script src="{{ asset('admin/vendors/@coreui/coreui/dist/js/coreui.min.js') }}"></script>
<script src="{{ asset('js/hammer.min.js') }}"></script>




</html>

