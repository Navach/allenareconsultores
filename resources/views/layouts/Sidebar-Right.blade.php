
<aside class="aside-menu">

    <!-- Tab panes-->

    <div class="tab-content">
        <div class="tab-pane active" id="timeline" role="tabpanel">
            <div class="list-group list-group-accent">
                <div
                    class="list-group-item list-group-item-accent-secondary bg-light text-center
                     font-weight-bold text-muted text-uppercase small  ">
                    {{ Auth::user()->correo }}</div>


                <a href="{{ route('UsuarioEdit',[ Auth::user()->id ]) }}">
                <div style="cursor: pointer" class="list-group-item list-group-item-accent-info w-100 dropdown-item" >
                   <i class="fa fa-user pr-3" ></i><strong class="colotLink" >Perfil</strong>
                </div>
                </a>
                <a  target="_blank"
                    href="{{ asset('ManualUsuario/Manual_Usuario_Allenare.pdf') }}">
                <div style="cursor: pointer" class="list-group-item list-group-item-accent-info w-100 dropdown-item" >

                        <i class="fa fa-book pr-3" ></i>
                        <strong class="colotLink">Manual de usuario</strong>
                </div>
                </a>

                <a href="" class="text-decoration-none" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">

                <div style="cursor: pointer" class="list-group-item list-group-item-accent-danger w-100 dropdown-item"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-lock pr-3"></i><strong class="colotLink">Cerrar Sesión</strong>

                    <form id="logout-form" method="POST" action="{{ route('logout') }}">
                        @csrf
                    </form>

                </div>
                </a>
            </div>
        </div>
    </div>
</aside>
