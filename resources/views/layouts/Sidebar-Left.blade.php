<div class="sidebar" >
    <nav class="sidebar-nav">
        <ul class="nav">


            <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="nav-icon fa fa-tachometer"></i> Dashboard
                </a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ route('UsuarioShow')  }}">
                    <i class="nav-icon fa fa-user-o"></i>Empleados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('newPro') }}">
                    <i class="nav-icon fa fa-id-card"></i> Registro Servicio
                </a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ route('Calendario') }}">
                    <i class="nav-icon fa fa-calendar"></i>Calendario</a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ route('linea') }}">
                    <i class="nav-icon fa fa-clock-o"></i>Linea De Tiempo</a>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
