<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="http://allenare.com.mx/inicio.php">

        <img class="navbar-brand-full" src="{{ asset('admin/img/brand/logooficioalallenare.PNG') }}"style="height: 56px; object-fit: scale-down"
             alt="Allenare">
        <img class="navbar-brand-minimized" src="{{ asset('admin/img/brand/logologo.PNG') }}" width="30" height="30"
             alt="Allenare">


    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    {{--
  <ul class="nav navbar-nav d-md-down-none">
      <li class="nav-item px-3">
          <a class="nav-link" href="#">Dashboard</a>
      </li>
      <li class="nav-item px-3">
          <a class="nav-link" href="#">Users</a>
      </li>
      <li class="nav-item px-3">
          <a class="nav-link" href="#">Settings</a>
      </li>
  </ul>
  --}}


    <ul class="nav navbar-nav ml-auto">
        {{--
         <li class="nav-item d-md-down-none">
             <a class="nav-link" href="#">
                 <i class="icon-list"></i>
             </a>
         </li>
         <li class="nav-item d-md-down-none">
             <a class="nav-link" href="#">
                 <i class="icon-location-pin"></i>
             </a>
         </li>
         --}}
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">

                @if(Storage::disk('public')->exists(Auth::user()->imagen))
                    @php($img = asset('storage').'/'. Auth::user()->imagen)
                @else
                    @php($img = asset('img/avatars/default-user.svg'))
                @endif

                <img class="img-avatar" src="{{ $img }}"
                     alt="{{ Auth::user()->correo }}">

            </a>

            <!-- Menu desplegable de perfil -->
          {{--   @include('layouts.Profile_drop-down_Menu')--}}


        </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
