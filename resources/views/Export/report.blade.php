<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body style="background-color: #8ad4ee;font-family: 'Roboto', sans-serif">

<div class="container">
    <h2 style="background-color: #0b4d75;color: white;font-family: 'Roboto', sans-serif">{{ $title }}</h2>
    <p>{{ $description }}</p>
    <table class="table" border="1">
        <thead>
        <tr>
            @foreach($headers as $header)
                <th>{{ $header }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)


            <tr>
                <td>{{ $row->nombre }}</td>
                <td>{{ $row->correo }}</td>
                <td>{{ $row->telefono }}</td>
                <td>{{ $row->direccion }}</td>
                <td>{{ $row->fechaIng }}</td>
                <td>{{ $row->conocimientos }}</td>
                <td>{{ $row-> color }}</td>
                <td>{{ $row->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>
