@extends('layout')
@section('contenido')

    <link rel="stylesheet" href="{{ asset('Calendar/fullcalendar.min.css') }}"/>

    <div class="card">
        <div class="card-header">
            <i class="fa fa-edit"></i><label>calendario</label>
        </div>
        <div class="card-body">

            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div id="calendario" class="col-sm-12">
                            <h2 class="mb-2">Detalle <b>Calendario</b></h2>
                            <div class="search-box ml-3 mb-4">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                <input type="date" id="dateField" class="form-control">

                            </div>

                            {!! $calendar->calendar() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! $calendar->script() !!}
    <script src="{{ asset('Calendar/moment.min.js') }}"></script>
    <script src="{{ asset('Calendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('Calendar/locale/es.js') }}"></script>
    <script src="{{ asset('Calendar/calendario.js') }}"></script>

@stop
