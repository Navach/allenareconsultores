
## INSTRUCCIÓNES


- Clonar el proyecto en una carpeta

- Ejecutar : composer install

- En la raiz del proyecto 

-Ejecutar : npm install 

-Ejecutar : npm audit fix

- Ir a public/admin
-Ejecutar : npm install

- Ejecutar (si no tiene .env ) :  cp .env.example .env 
- Ejecutar : php artisan key:generate 
- Ejecutar : php artisan storage:link  

- Ejecutar : php artisan migration install  
- Ejecutar : php artisan migration:refresh  
- Ejecutar : php artisan serve
