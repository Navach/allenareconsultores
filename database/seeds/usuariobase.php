<?php

use Illuminate\Database\Seeder;

class usuariobase extends Seeder
{


    public function run()
    {
        $data = [
            [   'nombre' => "Christian" ,
                'imagen' => "",
                'correo' => "josue23mem@hotmail.com",
                'contraseña' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
                'telefono' => '36063601',
                'direccion' => "P.sherman Administrator"
            ],
            //['title'=>'Devtalk', 'start_date'=>'2017-10-13', 'end_date'=>'2017-09-25'],
            //['title'=>'Super Event', 'start_date'=>'2017-09-23', 'end_date'=>'2017-09-24'],
            //['title'=>'wtf event', 'start_date'=>'2017-09-19', 'end_date'=>'2017-09-27'],
        ];
        \DB::table('usuario')->insert($data);

    }
}
