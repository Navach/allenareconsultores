<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProyectos extends Migration
{

    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_Proyecto');
            $table->string('nombre_Empresa');
            $table->string('Recursos_Requeridos');
            $table->string('alcance')-> nullable();
            $table->string('condiciones')-> nullable();
            $table->string('comentarios')-> nullable();
            $table->integer('horas_Programadas');
            $table->integer('horas_Realizadas');
            $table->integer('horas_Restantes')-> nullable();
            $table->string('status')-> nullable();
            $table->timestamp('fecha_Inicial');
            $table->timestamp('fecha_Final');
            $table->string('cronograma');
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
