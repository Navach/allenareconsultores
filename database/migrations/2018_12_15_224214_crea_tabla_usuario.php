<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaUsuario extends Migration
{

    public function up()
    {

        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('imagen');
            $table->string('correo',150)->unique();
            $table->string('contraseña');
            $table->string('telefono');
            $table->string('direccion');
            $table->string('color')->default('#81f781');
            $table->string('recordar_token')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
