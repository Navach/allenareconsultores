<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Usuario_tokens;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Usuario;

class UsuarioController extends Controller
{


    public function login_movil(Request $request)
    {
        if (Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'

        ])->fails()
        ) {
            return Response::json(array('error' => "No se enviaron los parámetros correctamente", 'exito' => false), 400);
        }

        $email = Input::get('email');
        $password = Input::get('password');
        $os = Input::get('os');

        $user = Usuario::where('correo', $email)->first();

        if ($user) {
            if (Hash::check($password, $user->contraseña)) {

                while (true) {

                    $token = str_random(60);
                    $session = Usuario_tokens::where('api_token')->first();
                    if (is_null($session)) {
                        Usuario_tokens::create(array(
                            'usuario_id' => $user->id,
                            'os' => $os,
                            'api_token' => $token,
                            'online' => true,
                        ));

                        return Response::json(array(
                            'data' => $user,
                            'token' => $token,
                            'exito' => true), 200);
                    }
                }
            }
        }

        return Response::json(array('error' => "Usuario/contraseña incorrecta", 'exito' => false), 400);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function getAll(Request $request)
    {

        $users = Usuario::orderBy('created_at', 'DESC')->paginate(5);


        foreach ($users as $user) {

            $user->direccion = str_limit($user->direccion, 30);

            if (Storage::disk('public')->exists($user->imagen)) {
                $user->imagen = asset('storage') . '/' . $user->imagen;
            } else {
                $user->imagen = asset('img/avatars/default-user.svg');
            }

        }

        return Response::json(array('data' => ['paginate' => ['total' => $users->total(),
            'current_page' => $users->currentPage(),
            'per_page' => $users->perPage(),
            'last_page' => $users->lastPage(),
            'from' => $users->firstItem(),
            'to' => $users->lastPage()],
            'users' => $users,],
            'exito' => true), 200);

    }


    public function getUsersAll()
    {

        $users = Usuario::orderBy('created_at', 'DESC')->get();


        foreach ($users as $user) {

            $user->direccion = str_limit($user->direccion, 30);

            if (Storage::disk('public')->exists($user->imagen)) {
                $user->imagen = asset('storage') . '/' . $user->imagen;
            } else {
                $user->imagen = asset('img/avatars/default-user.svg');
            }

        }

        return Response::json(array('data' => ['users' => $users]), 200);

    }


    public function search(Request $request)
    {

        $search = $request->search;

        $users = Usuario::where('nombre', 'like', "%$search%")
            ->Orwhere('nombre',     'like', "%$search%")
            ->Orwhere('correo',     'like', "%$search%")
            ->Orwhere('telefono',   'like', "%$search%")
            ->Orwhere('direccion',  'like', "%$search%")
            ->Orwhere('created_at', 'like', "%$search%")
            ->orderBy('created_at', 'DESC')
            ->paginate(5);

        foreach ($users as $user) {
            $user->direccion = str_limit($user->direccion, 30);

            if (Storage::disk('public')->exists($user->imagen)) {
                $user->imagen = asset('storage') . '/' . $user->imagen;
            } else {
                $user->imagen = asset('img/avatars/default-user.svg');
            }

        }

        return Response::json(array('data' => ['paginate' => ['total' => $users->total(),
            'current_page' => $users->currentPage(),
            'per_page' => $users->perPage(),
            'last_page' => $users->lastPage(),
            'from' => $users->firstItem(),
            'to' => $users->lastPage()],
            'users' => $users,],
            'exito' => true), 200);

    }

    public function getUser(Request $request)
    {
        $u = Usuario::where('api_token', $request->token)->first();
        if ($u) {
            return Response::json(array('data' => $u, 'exito' => true), 200);
        }
        return Response::json(array('error' => "Sin resultados", 'exito' => false), 400);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), array(
            'nombre' => 'required',
            'correo' => 'required|email',
            'contraseña' => 'required',
            'confirmar_contraseña' => 'required',
            'teléfono' => 'required',
            'dirección' => 'required',
            'imagen' => 'required',
            'color' => 'required',
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
        }


        if ($request->contraseña != $request->confirmar_contraseña) {
            return Response::json(array('error' => "Las contraseñas no coinsiden", 'exito' => false), 400);
        } else {
            $contrasena = Hash::make($request->contraseña);
        }

        $usuario = Usuario::where('correo', $request->correo)->first();
        if (!is_null($usuario)) {
            return Response::json(array('error' => "correo existente", 'exito' => false), 400);

        }

        $usuario = Usuario::create([
            'nombre' => $request->nombre,
            'imagen' => $request->file('imagen')->store('usuarios_img'),
            'correo' => $request->correo,
            'contraseña' => $contrasena,
            'telefono' => $request->teléfono,
            'direccion' => $request->dirección,
            'color' => $request->color,
            'conocimientos' => $request->conocimientos,
            'fechaIng' => $request->fechaIng,

        ]);

        if ($usuario) {
            return Response::json(array('data' => "Registro exitoso", 'exito' => true), 200);
        } else {
            return Response::json(array('error' => "Fallo al registrar intentalo mas tarde", 'exito' => false), 400);
        }

    }

    public function update(Request $request, $id)
    {

        $user = Usuario::find($id);


        if ($user) {

            if ($request->has('nombre')) {

                $validator = Validator::make($request->all(), array('nombre' => 'required'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->nombre = $request->nombre;
            }


            if ($request->has('color')) {

                $validator = Validator::make($request->all(), array('color' => 'required',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->color = $request->color;
            }



            if ($request->has('correo')) {

                $validator = Validator::make($request->all(), array('correo' => 'required|email',));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->correo = $request->correo;
            }

            if ($request->has('teléfono')) {
                $validator = Validator::make($request->all(), array('teléfono' => 'required'));

                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->telefono = $request->teléfono;
            }

            if ($request->has('dirección')) {
                $validator = Validator::make($request->all(), array('dirección' => 'required'));

                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                $user->direccion = $request->dirección;
            }

            if ($request->has(['contraseña', 'confirmar_contraseña'])) {

                $validator = Validator::make($request->all(), array(
                    'contraseña' => 'required',
                    'confirmar_contraseña' => 'required'));
                if ($validator->fails()) {
                    return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
                }

                if ($request->contraseña === $request->confirmar_contraseña) {

                    $user->contraseña = Hash::make($request->contraseña);
                } else {
                    return Response::json(array(
                        'error' => "La nueva contraseña no coinside",
                        'exito' => false), 400);

                }
            }

            if ($request->has('imagen')) {

                if (Storage::disk('public')->exists($user->imagen))
                    Storage::disk('public')->delete($user->imagen);

                $user->imagen = $request->file('imagen')->store('usuarios_img');

            }

            $user->save();


            return Response::json(array('data' => Usuario::getPathImage($user), 'exito' => true), 200);


        } else {
            return Response::json(array(
                'error' => "Usuario no encontrado",
                'exito' => false), 400);

        }
    }

    public function delete($user_id)
    {

        $user = Usuario::find($user_id);
        if ($user) {

            if (Storage::disk('public')->exists($user->imagen))
                Storage::disk('public')->delete($user->imagen);

            $user->delete();
            return Response::json(array('data' => 'ok', 'exito' => true), 200);
        }

        return Response::json(array('error' => "Sin resultados", 'exito' => false), 400);

    }
}
