<?php

namespace App\Http\Controllers\Api;

use App\Models\Proyectos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ProjectsControllers extends Controller
{


    function nuevo(Request $request)
    {


        $validator = Validator::make($request->all(), array(
            'nombre_Proyecto' => 'required|min:3|max:55',
            'nombre_Empresa' => 'required|min:3|max:55',
            'Recursos_Requeridos' => 'required|min:3|max:55',
            'alcance' => 'min:3|max:55',
            'condiciones' => 'min:3|max:55',
            'comentarios' => 'min:3|max:55',
            'horas_Programadas' => 'required|numeric',
            'horas_Realizadas' => 'required|numeric',
            'fecha_Inicial' => 'required|date',
            'fecha_Final' => 'required|date',
            'encargado' => 'required',
            'cronograma' => 'required|file',
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }

        if($request->input('horas_Realizadas') > $request->input('horas_Programadas')){
            return Response::json(array('success' => 'Las horas realizadas superan a las horas programadas'), 200);
        }


        if($request->input('fecha_Inicial') > $request->input('fecha_Final')){
            return Response::json(array('error' => 'La fecha inicial es mayor a la fecha final'), 400);
        }


        $proyecto = new Proyectos();

        $proyecto->nombre_Proyecto=  $request->input('nombre_Proyecto');
        $proyecto->nombre_Empresa=  $request->input('nombre_Empresa');
        $proyecto->Recursos_Requeridos=  $request->input('Recursos_Requeridos');
        $proyecto->alcance=  $request->input('alcance');
        $proyecto->condiciones=  $request->input('condiciones');
        $proyecto->comentarios=  $request->input('comentarios');
        $proyecto->horas_Programadas=  $request->input('horas_Programadas');
        $proyecto->horas_Realizadas=  $request->input('horas_Realizadas');
        $proyecto->horas_Restantes=  $request->input('horas_Restantes');
        $proyecto->fecha_Inicial=  $request->input('fecha_Inicial');
        $proyecto->fecha_Final=  $request->input('fecha_Final');
        $proyecto->usuario_id=  $request->input('encargado');
        $proyecto->cronograma= $request->file('cronograma')->store('cronogramas');

        $proyecto->save();



        if ($proyecto) {
            return Response::json(array('success' => $proyecto), 200);
        }


        return Response::json(array('error' => 'Opps intentelo mas tarde'), 400);


    }

    public function destroy($id)
    {
        $Proyecto = Proyectos::where('id', $id)->first();

        if ($Proyecto) {
            $Proyecto->delete();
        }
        return Response::json(array("success" => 'Eliminado'), 200);
    }


}
