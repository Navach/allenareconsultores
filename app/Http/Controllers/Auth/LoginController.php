<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Usuario_tokens;
use App\Usuario;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'email' => 'required|email',
            'password' => 'required'
        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors(), 'exito' => false), 422);
        }


        $email = Input::get('email');
        $password = Input::get('password');
        $os = Input::get('os');

        $user = Usuario::where('correo', $email)->first();

        if ($user) {
            if (Hash::check($password, $user->contraseña)) {

                $this->guard()->login($user);
                return Response::json(array(
                    'data' => Auth::user(), 'exito' => true), 200);
            }
        }

        return Response::json(array('error' => "Usuario/contraseña incorrecta", 'exito' => false), 400);
    }

    public function redirectToProvider(string $driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    public function handleProviderCallback(string $driver)
    {
        $socialUser = Socialite::driver($driver)->user();
        dd($socialUser);

    }

    protected function credentials(Request $request)
    {
        return [
            'correo' => $request->get('email'),
            'contraseña' => $request->password
        ];
    }

    public function username()
    {
        return 'correo';
    }

}
