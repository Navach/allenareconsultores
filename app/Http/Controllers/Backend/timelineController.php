<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\timeline;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class timelineController extends Controller
{

    public function show()
    {
        $lineadetiempo = timeline::orderBy('fecha', 'DESC')->get();

        foreach ($lineadetiempo as $tiempo) {
            $tiempo->Imagen = asset("storage") . "/" . $tiempo->Imagen;
        }

        return Response::json(array("success" => $lineadetiempo), 200);

    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), array(
            'fecha' => 'required',
            'titulo' => 'required',
            'description' => 'required',
            'Imagen' => 'required',

        ));

        if ($validator->fails()) {
            return Response::json(array('error' => $validator->errors()), 422);
        }


        $timeline = new timeline();

        $timeline->fecha = $request->input('fecha');
        $timeline->titulo = $request->input('titulo');
        $timeline->description = $request->input('description');
        $timeline->Imagen = $request->file('Imagen')->store('timeslines');

        $timeline->save();

        if ($timeline) {
            return Response::json(array('success' => $timeline), 200);
        }


        return Response::json(array('error' => 'Opps intentelo de nuevo'), 400);


    }

    public function destroy($id)
    {
        $lineadetiempo = timeline::where('id', $id)->first();

        if ($lineadetiempo) {
            $lineadetiempo->delete();
        }
        return Response::json(array("success" => 'Eliminado'), 200);
    }


}
