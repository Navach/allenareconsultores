<?php

namespace App\Http\Controllers\Backend;

use App\Exports\UsuariosExcel;
use App\Usuario;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;


class UsuarioController_View extends Controller
{


    public function perfil()
    {
        return view('Backend.Docs.perfil')->withPage('perfil');

    }

    public function manual()
    {
        return view('Backend.Docs.manual')->withPage('manual');

    }

    public function view_login()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('auth.login');
    }

    public function view_show()
    {
        return view('Backend.Usuarios.show')->withPage('Usuarios');
    }

    public function view_edit($id)
    {

        $user = Usuario::find($id);
        if ($user) {
            $user = Usuario::getPathImage($user);

            $page = 'Usuario-Editar';
            $parameter = $id;
            return view('Backend.Usuarios.form', compact('user', 'page', 'parameter'));
        }
        return redirect()->route('UsuarioShow')->withPage('Usuarios');

    }

    public function view_add()
    {
        return view('Backend.Usuarios.form')->withPage('Usuario-Agregar');
    }

    public function exportExcel()
    {
        return Excel::download(new UsuariosExcel(), 'reporte.xlsx');
    }

    public function exportPDF()
    {
        $title = "Reporte de usuarios";
        $description = "Reporte de backend sobre los usuarios";
        $headers = ['Nombre', 'Correo', 'Teléfono', 'Dirección', 'Fecha Registro'];
        $rows = Usuario::all();

        $pdf = PDF::loadView('Export.report',
                    compact('headers', 'rows', 'title', 'description'))
            ->setPaper('a4', 'landscape');

        return $pdf->download('reporte.pdf');

    }

}
