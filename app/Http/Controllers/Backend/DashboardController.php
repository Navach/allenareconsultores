<?php

namespace App\Http\Controllers\Backend;

use App\Models\Proyectos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function show()
    {
        return view('dashboard')->withPage('Dashboard');
    }

    public function newProject()
    {


        return view('Backend.Proyecto.newPro')->withPage('agregar Proyecto');
    }

    public function editProject($id)
    {



        $proyecto = Proyectos::find($id);

        $proyecto->cronograma =  asset('storage/'.$proyecto->cronograma);
        $proyecto->usuario;

        $parameter = $id;

        $page = 'Ver Proyecto';

        return view('Backend.Proyecto.newPro', compact( 'proyecto','page', 'parameter'));
    }

}
