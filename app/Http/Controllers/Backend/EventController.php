<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Proyectos;
use App\Usuario;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class EventController extends Controller
{
    public function index()
    {
        $events = [];
        // $data = Event::all();
        $data = Proyectos::all();


        if ($data) {

            foreach ($data as $key => $value) {

                $usuario = Usuario::find($value->usuario_id);


                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->fecha_Inicial),
                    new \DateTime($value->fecha_Final . ' +1 day'),
                    null,
                    // Add color and link on event
                    [
                        'color' => !isset($usuario->color) ? '#81f781' : $usuario->color,
                        'url' => route('editPro', $value->id),
                        'textColor' => '#fff',
                        'background' => '#04B4AE',
                    ]
                );

            }
        }
        $calendar = Calendar::addEvents($events);


        return view('Backend.Calendario.Calendario', compact('calendar'))->withPage('Calendario');

    }

    public function linea()
    {
        return view('Backend.Calendario.Linea')->withPage('linea');

    }



}


