<?php

namespace App\Exports;

use App\Usuario;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsuariosExcel implements FromView
{

    public function view():View
    {
        $title = "Reporte de usuarios";
        $description = "Reporte de backend sobre los usuarios";
        $headers = ['Nombre', 'Correo', 'Teléfono', 'Dirección', 'Fecha Registro'];
        $rows = Usuario::all();

        return view('Export.report',compact('headers', 'rows', 'title', 'description'));

    }
}
