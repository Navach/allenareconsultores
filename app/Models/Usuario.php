<?php

namespace App;

use App\Models\Usuario_tokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = "usuario";

    protected $username = 'username';

    protected $fillable = [
        'nombre',
        'imagen',
        'correo',
        'contraseña',
        'telefono',
        'direccion',
        'color',
        'conocimientos',
        'fechaIng',
    ];

    protected $hidden = ['contraseña'];


    public static function getPathImage($user)
    {

        if (Storage::disk('public')->exists($user->imagen)) {
            $user->imagen = asset('storage') . '/' . $user->imagen;
        } else {
            $user->imagen = asset('img/avatars/default-user.svg');
        }

        return $user;


    }

    public function tokens()
    {
        return $this->hasMany(Usuario_tokens::class);
    }

    public function getRememberTokenName()
    {
        return 'recordar_token';
    }


}
