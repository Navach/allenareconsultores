<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario_tokens extends Model
{
    protected $table = "usuario_tokens";

    protected $fillable = [
        'usuario_id',
        'os',
        'api_token',
        'online',
    ];

    protected $hidden = [];

    public function usuario()
    {
        return $this->belongsTo('App\Usuario');
    }

}
