<?php

namespace App\Models;

use App\Usuario;
use Illuminate\Database\Eloquent\Model;

class Proyectos extends Model
{

    protected $fillable = [
        'nombre_Proyecto',
        'nombre_Empresa',
        'Recursos_Requeridos',
        'horas_Programadas',
        'horas_Realizadas',
        'horas_Restantes',
        'fecha_Inicial',
        'fecha_Final',
        'cronograma',
        'usuario_id',
    ];


    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }



}
