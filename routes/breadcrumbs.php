<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;


// dashboard
Breadcrumbs::for('Dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});

// dashboard > usuarios
Breadcrumbs::for('Usuarios', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Usuario', route('UsuarioShow'));
});

// dashboard > usuarios > agregar
Breadcrumbs::for('Usuario-Agregar', function ($breadcrumbs) {
    $breadcrumbs->parent('Usuarios');
    $breadcrumbs->push('Agregar', route('UsuarioAdd'));
});

// dashboard > usuarios > editar > [id]
Breadcrumbs::for('Usuario-Editar', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('Usuarios');
    $breadcrumbs->push('Editar', route('UsuarioEdit',$id));

});


// dashboard > proyectos > editar > [id]
Breadcrumbs::for('Ver Proyecto', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Ver Proyecto', route('UsuarioEdit', $id));

});

// dashboard > calendario
Breadcrumbs::for('Calendario', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('Calendario', route('Calendario'));
});

// dashboard > calendario > LineaDeTiempo
Breadcrumbs::for('linea', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('linea', route('linea'));
});

// dashboard > calendario > LineaDeTiempo
Breadcrumbs::for('agregar Proyecto', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('agregar Proyecto', route('newPro'));
});


// dashboard > perfil
Breadcrumbs::for('perfil', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('perfil', route('perfil'));
});
// dashboard > manual
Breadcrumbs::for('manual', function ($breadcrumbs) {
    $breadcrumbs->parent('Dashboard');
    $breadcrumbs->push('manual', route('manual'));
});

