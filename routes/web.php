<?php


Auth::routes();

Route::get('login/{driver}','Auth\LoginController@redirectToProvider')->name('social_auth');

Route::get('login/{driver}/callback','Auth\LoginController@handleProviderCallback');

Route::get('/', 'Backend\UsuarioController_View@view_login')->name('view_login');


Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', 'Backend\DashboardController@show')->name('dashboard');

    Route::group(['prefix' => 'usuarios'], function () {
        $controller = 'Backend\UsuarioController_View@';

        Route::get('/', $controller . 'view_show')->name('UsuarioShow');
        Route::get('/agregar', $controller . 'view_add')->name('UsuarioAdd');
        Route::get('/editar/{id}', $controller . 'view_edit')->name('UsuarioEdit');
        Route::get('/exportExcel', $controller . 'exportExcel')->name('UsuarioExcel');
        Route::get('/exportPDF', $controller . 'exportPDF')->name('exportPDF');

        Route::get('vinculados/importar', 'ImpvinculadosController@getImport');

        Route::post('vinculados/importar/iniciar', 'ImpvinculadosController@postImport');

    });

    Route::get('/manual', 'Backend\UsuarioController_View@manual')->name('manual');//ruta para manual
    Route::get('/perfil', 'Backend\UsuarioController_View@perfil')->name('perfil');// ruta para perfil

    Route::get('/calendario', 'Backend\EventController@index')->name('Calendario');

    Route::get('/Lineadetiempo', 'Backend\EventController@linea')->name('linea');

    Route::get('/Proyecto', 'Backend\DashboardController@newProject')->name('newPro');
    Route::get('/Proyecto/{id}', 'Backend\DashboardController@editProject')->name('editPro');


});

