<?php

use Illuminate\Http\Request;


Route::group(['prefix' => 'user'], function () {

    $controller = 'Api\UsuarioController@';

    Route::post('/login_movil', $controller . 'login_movil');
    Route::get('/',    $controller . 'getAll');
    Route::get('/getUsersAll',    $controller . 'getUsersAll');
    Route::post('/register',    $controller . 'register');
    Route::patch('/update/{id}',    $controller . 'update');
    Route::post('/search',        $controller . 'search');
    Route::delete('/delete/{user_id}',    $controller . 'delete');

    Route::post('/{id}',     $controller . 'getUser');
});

Route::group(['prefix' => 'timeline'], function () {

    $controller = 'Backend\timelineController@';

    Route::get('/',    $controller . 'show');
    Route::post('/create',    $controller . 'create');
    Route::delete('/destroy/{id}',    $controller . 'destroy');

});



Route::group(['prefix' => 'proyecto'], function () {

    $controller = 'Api\ProjectsControllers@';

    Route::post('/nuevo', $controller . 'nuevo');
    Route::delete('/destroy/{id}',    $controller . 'destroy');

});


